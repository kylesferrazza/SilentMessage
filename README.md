# SilentMessage
This plugin allows players to send messages to each other with a very minimalistic layout.

## Commands
silentmessage:
<br>&nbsp;&nbsp;&nbsp;&nbsp;description: Sends a message to a player without any text before or after the message.
<br>&nbsp;&nbsp;&nbsp;&nbsp;alias: sm
<br>&nbsp;&nbsp;&nbsp;&nbsp;permission: silentmessage.send
<br>&nbsp;&nbsp;&nbsp;&nbsp;usage: "/sm &lt;player&gt; &lt;message &#40;supports color codes&#41;&gt;"

## Permissions
<pre>silentmessage.send</pre> allows a player to use /silentmessage.
<br>The console can use this command automatically.

## Config
This plugin does not require any configuration.
