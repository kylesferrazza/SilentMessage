package io.github.tubakyle.silentmessage;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Kyle Sferrazza on 8/8/2014.
 * This file is a part of: SilentMessage.
 * All rights reserved.
 */
public class SMCommands implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(cmd.getName().equalsIgnoreCase("silentmessage"))) return true;
        if(args.length <= 1) return false;
        Player sendMsgTo = null;
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            if (args[0].equalsIgnoreCase(onlinePlayer.getName())) {
                sendMsgTo = onlinePlayer;
            }
        }
        if (sendMsgTo == null) {
            sender.sendMessage(ChatColor.RED + "Invalid player.");
            return true;
        }
        String message = "";
        int counter = 1;
        for (String argument : args) {
            if (counter != 1) message = message + " " + argument;
            counter++;
        }
        if (message.isEmpty()) return false;
        sendMsgTo.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
        return true;
    }
}
