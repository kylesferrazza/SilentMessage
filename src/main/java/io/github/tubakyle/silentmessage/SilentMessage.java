package io.github.tubakyle.silentmessage;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Kyle Sferrazza on 8/8/2014.
 * This file is a part of: SilentMessage.
 * All rights reserved.
 */
public class SilentMessage extends JavaPlugin {
    @Override
    public void onEnable() {
        getCommand("silentmessage").setExecutor(new SMCommands());
    }
}
